<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('users', ['uses' => 'UserController@create']);

$router->group(['middleware' => 'auth:api'], function () use ($router)
{
    $router->get('users',  ['uses' => 'UserController@index']);

    $router->get('users/{id}', ['uses' => 'UserController@show']);

    $router->put('users/{id}', ['uses' => 'UserController@update']);

    $router->delete('users/{id}', ['uses' => 'UserController@delete']);
});

$router->group(['prefix' => 'users'], function () use ($router)
{
    $router->get('/key', function() {
        return str_random(32);
    });

    $router->post('/auth/login', 'AuthController@postLogin');
});