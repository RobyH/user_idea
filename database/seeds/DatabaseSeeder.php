<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        DB::table('users')->insert([
            'name' => 'Juan Pablo Bascope',
            'email' => 'pabloblass@gmail.com',
            'password' => app('hash')->make('123456'),
            'remember_token' => str_random(10),
        ]);

        factory(App\User::class, 20)->create();
    }
}
