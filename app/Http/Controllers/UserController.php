<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function index()
    {
        return response()->json(User::all(), 200);
    }

    public function show($id)
    {
        $user = User::find($id);

        if($user)
        {
            return response()->json($user, 200);
        }

        return response()->json(["Usuario no encontrado"], 404);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:150',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:6|max:255|confirmed'
        ]);

        //$user = User::create($request->all());
        $user = new User;
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = app('hash')->make($request->get('password'));
        //$user->remember_token = $request->get('remember_token');
        $user->save();

        return response()->json($user, 201);
    }

    public function update($id, Request $request)
    {
        $user = User::findOrFail($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = app('hash')->make($request->get('password'));
        $user->update();

        return response()->json($user, 200);
    }

    public function delete($id)
    {
        User::findOrFail($id)->delete();
        return response('Eliminacion Exitosa', 200);
    }
}